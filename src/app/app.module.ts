import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactsComponent } from './contacts/contacts.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { HomeComponent } from './home/home.component';
import {RouterModule, Routes} from "@angular/router";
import { ModalComponent } from './ui/modal/modal.component';
import {NoClickComponent} from "./no-click.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'about/us', component: AboutUsComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: '**', component: NoClickComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    ContactsComponent,
    AboutUsComponent,
    ToolbarComponent,
    HomeComponent,
    ModalComponent,
    NoClickComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
