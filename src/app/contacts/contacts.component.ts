import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent {

  modalOpen = false;

constructor(
  private router: Router,
) {}
  openCheckoutModal(){
    this.modalOpen = true;
  }

  closeCheckoutModal(){
    this.modalOpen = false;
  }

   noClick(){
  void this.router.navigate(['no']);
  }
}
