import {Component} from "@angular/core";

@Component({
  selector: 'app-noclick',
  template: `<h1>Sorry we could not provide full info!!!</h1>
  <div>You can contact to our manager</div>
  <div><i>Saikal Muratbek kyzy</i></div>
  <div class="email"><i>saikalm@gmail.com</i></div>
  `,
  styles: [`
  h1{
    color: red;
  }
  .email{
    color:blue;
  }
  `]
})

export class NoClickComponent {

}
